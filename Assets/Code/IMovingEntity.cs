using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.BeatEmUp
{
    /// <summary>
    /// Creation Date:   5/21/2021 10:38:50 PM
    /// Product Name:    BeatEmUp
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     This will be shared between several objects that can move.
    /// Changelog:         
    /// </summary>
    public interface IMovingEntity
    {
        Vector3 Position { get; }
        Vector3 Movement { get; }
        
    }
}