using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.BeatEmUp
{
    /// <summary>
    /// Creation Date:   5/9/2021 6:05:21 PM
    /// Product Name:    BeatEmUp
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    public interface IInput
    {
        Vector2 intention { get; }
        bool attack { get; set; }
        bool attack2 { get; set; }
        bool jump { get; set; }
        int aiRequestedIdx { get; set; }
    }
}