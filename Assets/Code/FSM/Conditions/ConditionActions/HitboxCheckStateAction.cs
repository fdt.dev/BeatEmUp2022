using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   2/9/2022 7:28:57 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public class HitboxCheckStateAction : StateActionBase
    {
        [SerializeField] protected string _customPainStateForReceiver;
        [SerializeField] protected int _attackDamage;
        
        public override void Execute(BEUFighter e)
        {
            var o =e.HitBox.CheckCollision();
            e.IsGrab = false;
            e.SetAttackDamage(_attackDamage);
            
            if (o != null && o != e && o.LastAttackReceived != e.HitBox.HitStateID)
            {
                if (!string.IsNullOrEmpty(_customPainStateForReceiver))
                {
                    if (o is BEUFighter bTarget)
                    {
                        bTarget.SetState(_customPainStateForReceiver);
                    }
                }
                else
                {
                    o.CollidedWithHitbox(e.HitBox, e);
                }
            }
        }
    }
}