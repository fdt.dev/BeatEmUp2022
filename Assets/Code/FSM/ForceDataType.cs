namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   1/5/2022 9:13:29 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    public enum ForceDataType
    {
        DISABLED= 0, ON_INIT = 1, ON_UPDATE = 2
    }
}