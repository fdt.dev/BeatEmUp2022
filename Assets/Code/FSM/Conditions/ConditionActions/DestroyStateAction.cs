using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   2/9/2022 7:28:57 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public class DestroyStateAction : StateActionBase
    {
        public override void Execute(BEUFighter e)
        {
            GameObject.Destroy(e.gameObject);
        }
    }
}