using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   1/3/2022 4:13:44 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [Juce.ImplementationSelector.SelectImplementationCustomDisplayName("Get Hit in Back")]
    public class GetHitInBackCondition : ConditionBase
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        
        #endregion
                        
        #region Methods

        public override bool CheckCondition(BEUFighter e)
        {
            return (e.HasBeenHit && IsBack(e, e.HitData.Owner));
        }

        private bool IsBack(BEUFighter victim, BEUObject attacker)
        {
            if (victim.facingDirection == 1 && attacker.transform.position.x < victim.Position.x)
                return true;
            if (victim.facingDirection == -1 && attacker.transform.position.x > victim.Position.x)
                return true;
            return false;
        }

        #endregion
    }
}