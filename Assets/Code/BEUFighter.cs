using System.Collections;
using System.Collections.Generic;
using com.FDT.BeatEmUp.FSM;
using UnityEngine;

namespace com.FDT.BeatEmUp
{
    /// <summary>
    /// Creation Date:   1/3/2022 7:12:05 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     Main character or npc script. Handles FSM asset, physics scripts, and data.
    /// Changelog:         
    /// </summary>
    public class BEUFighter : BEUObject, IMovingEntity
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        public RigidbodyWrapper rb;
        [SerializeField] protected BEUAnimatorHook _anim;
        [SerializeField] protected FSMDataAsset _asset;
        [SerializeField] protected InputAsset _inputAsset;
        [SerializeField] protected Transform _boundingBoxes;
        [SerializeField] protected Hitbox _hitBox;
        [SerializeField] protected GameObject _boundingBox;

        [SerializeField] protected string _currentState;
        #endregion

        #region Properties, Consts and Statics

        public Hitbox HitBox => _hitBox;
        public GameObject BoundingBox => _boundingBox;

        public Vector2 Intention => _inputAsset.intention;
        public Vector3 Position
        {
            get => transform.position;
        }

        public Vector3 Movement
        {
            get => rb.velocity;
        }

        public bool AttackRequested
        {
            get => _inputAsset.attack;
            set => _inputAsset.attack = value;
        }
        public bool Attack2Requested
        {
            get => _inputAsset.attack2;
            set => _inputAsset.attack2 = value;
        }
        public float CurrentStateTime
        {
            get
            {
                return Time.time - _currentStateInitTime;
            }
        }
        public bool JumpRequested
        {
            get => _inputAsset.jump;
            set => _inputAsset.jump = value;
        }

        public BEUAnimatorHook Anim
        {
            get => _anim;
        }
       
        #endregion

        #region Variables
        protected float _currentStateInitTime;
        #endregion

        #region Public API
        public void SetMovementSpeed(Vector2 intention)
        {
            rb.SetIntention(intention);
        }

        #endregion
                        
        #region Methods
        private void FaceIntentionDirection()
        {
            FaceDirection(_inputAsset.intention.x);
            _boundingBoxes.localEulerAngles = new Vector3(0, facingDirection == 1?0:180,0 );
        }
        private void RefreshFlip()
        {
            var state = _asset.fsm.GetStateData(_currentState);
            if (!state.CanFlip)
                return;
            
            if (Mathf.Abs(_inputAsset.intention.x) > InputAsset.DEADZONE)
                FaceIntentionDirection();
        }
        public void SetState(string newState)
        {
            var state = _asset.fsm.GetStateData(_currentState);
            if (state != null)
                state.EndState(this);
            _currentState = newState;
            InitCurrentState();
        }

        private void InitCurrentState()
        {
            _currentStateInitTime = Time.time;
            var state = _asset.fsm.GetStateData(_currentState);
            state.PlayAnimation(_anim);
            
            //_anim.Play(state.GetAnimation());
            state.InitState(this);
        }
        private void InitStateMachine()
        {
            _health = _asset.Health;
            InitCurrentState();
        }

        protected override void Update()
        {
            base.Update();
            float delta = Time.deltaTime;
            var state = _asset.fsm.GetStateData(_currentState);
            state.ExecuteState(this);
            
            RefreshFlip();
            string newState;
            state.CheckConditions(this, out newState);

            AttackRequested = false;
            Attack2Requested = false;
            JumpRequested = false;
        }

        public override void CollidedWithHitbox(Hitbox hitbox, BEUObject attacker)
        {
            var state = _asset.fsm.GetStateData(_currentState);
            state.CollidedWithHitbox(hitbox, this, attacker);
        }

        private void OnEnable()
        {
            InitStateMachine();
        }
        #endregion

        public bool HasState(string s)
        {
            return _asset.fsm.HasState(s);
        }
    }
}