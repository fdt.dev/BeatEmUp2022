﻿using UnityEngine;

namespace com.fdt.multitouch
{
    [System.Serializable]
    public class TouchData
    {
        public int fingerId;
        public bool exists;
        public Vector2 initialPos;
        public Vector2 lastPos;
        public Vector2 currentPos;
        public Vector2 deltaPos;
        public TouchPhase phase;
        public bool isDrag;
        public float initialRealTime;
        public bool moved;
        public bool canBeClick;
        public bool canBeLongPress;

    }
}