namespace StringFSMSystem
{
    public interface IStringFSM<T2> where T2:BaseFSMState
    {
        StringFSM<T2> fsm { get; }
    }
}