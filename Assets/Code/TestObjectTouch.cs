using System;
using com.FDT.BeatEmUp;
using com.fdt.multitouch;
using UnityEngine;

namespace defaultNamespace
{
    /// <summary>
    /// Creation Date:   4/22/2021 9:37:10 PM
    /// Product Name:    FDTModules
    /// Developers:      franc
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    public class TestObjectTouch : MonoBehaviour
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        [SerializeField] protected Transform joystickBack;
        [SerializeField] protected Transform joystick;
        [SerializeField] protected Transform attack;
        [SerializeField] protected float centerDistance = 10;
        [SerializeField] protected InputAsset _inputAsset;
        [SerializeField] protected RectTransform _specialZone;
        [SerializeField] protected RectTransform _jumpZone;
        private int draggingIdx = -1;
        private float attackTime = 0;
        [SerializeField] private float attTimeDelay = 0.15f;

        #endregion

        #region Properties, Consts and Statics

        #endregion

        #region Variables

        #endregion

        #region Public API

        private void Update()
        {
            float t = Time.time;
            if (attack.gameObject.activeInHierarchy && t > attackTime)
            {
                attack.gameObject.SetActive(false);
            }

            if (Input.GetKeyUp(KeyCode.A))
            {
                _inputAsset.attack = true;
            }
        }

        public bool IsInside(RectTransform rectTransform, Vector3 mousePosition)
        {
             Vector2 localPoint;
             RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, mousePosition, Camera.main, out localPoint);
             return rectTransform.rect.Contains(localPoint);
        }

        public void HandleTouch(TouchEventTypeNum t, TouchData d)
        {
            if (draggingIdx == -1 && t == TouchEventTypeNum.BEGIN_DRAG)
            {
                draggingIdx = d.fingerId;
                joystickBack.gameObject.SetActive(true);
                joystick.gameObject.SetActive(true);
                CenterIntoPosition(d, joystickBack);
                CenterIntoPosition(d, joystick);
            }

            if ((draggingIdx == -1 && t == TouchEventTypeNum.CLICK) ||
                (draggingIdx != -1 && t == TouchEventTypeNum.PRESSED) )
            {
                if (IsInside(_specialZone, d.currentPos))
                {
                    _inputAsset.attack2 = true;
                }
                else if (IsInside(_jumpZone, d.currentPos))
                {
                    _inputAsset.jump = true;
                }
                else
                {
                    _inputAsset.attack = true;    
                }
                attack.gameObject.SetActive(true);
                attackTime = Time.time + attTimeDelay;
                CenterIntoPosition(d, attack);
            }
            if (d.fingerId == draggingIdx)
            {
                if (t== TouchEventTypeNum.END_PRESS)
                {
                    joystickBack.gameObject.SetActive(false);
                    joystick.gameObject.SetActive(false);
                    _inputAsset.intention = Vector2.zero;
                    draggingIdx = -1;
                }
                else
                {
                    CenterIntoPosition(d, joystick);
                    //Translate(d, joystick);
                    Vector3 dist = joystick.position - joystickBack.position;
                    if (dist.magnitude > centerDistance)
                    {
                        dist = Vector3.ClampMagnitude(dist, centerDistance);
                        Vector3 newPos = joystick.position - dist;
                        joystickBack.position = newPos;
                    }
                    Vector2 intention = dist;
                    _inputAsset.intention = intention / centerDistance;
                }
            }
        }
        #endregion

        #region Methods

        public void Translate(TouchData t, Transform obj)
        {
            var delta = t.deltaPos;
            var lastPos = t.currentPos - t.deltaPos;
            bool a = RectTransformUtility.ScreenPointToWorldPointInRectangle(obj as RectTransform, lastPos,
                Camera.main, out Vector3 lastPoint);
            bool b = RectTransformUtility.ScreenPointToWorldPointInRectangle(obj as RectTransform, t.currentPos,
                Camera.main, out Vector3 currentPoint);
            if (a && b)
            {
                Vector3 o = currentPoint - lastPoint;
                obj.position += o;
            }
        }
        //[ContextMenu("call from context menu")]
        //public void Test() { }        
        public void CenterIntoPosition(TouchData data, Transform obj)
        {
            bool b = RectTransformUtility.ScreenPointToWorldPointInRectangle(obj as RectTransform, data.currentPos,
                Camera.main, out Vector3 currentPoint);
            if (b)
            {
                obj.position = currentPoint;
            }
        }
        #endregion
    }
}