using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   1/3/2022 4:13:44 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [Juce.ImplementationSelector.SelectImplementationCustomDisplayName("Timed")]
    public class ConditionTimed : ConditionBase
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        [SerializeField] protected float _minStateTime;
        #endregion

        #region Properties, Consts and Statics

        #endregion

        #region Variables

        #endregion

        #region Public API
        public override bool CheckCondition(BEUFighter beuFighter)
        {
            return beuFighter.CurrentStateTime >= _minStateTime;
        }       
        #endregion

        #region Methods

        //[ContextMenu("call from context menu")]
        //public void Test() { }        

        #endregion
    }
}