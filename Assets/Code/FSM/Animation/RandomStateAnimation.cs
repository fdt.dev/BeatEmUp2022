using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   2/17/2022 9:42:36 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public class RandomStateAnimation : StateAnimationBase
    {
        [SerializeField] protected List<string> _animationState = new List<string>();

        public override void Execute(BEUAnimatorHook anim)
        {
            var a = _animationState[Random.Range(0, _animationState.Count)];
            anim.Play(a);
        }
    }
}