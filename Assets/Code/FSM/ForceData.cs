using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   1/3/2022 6:48:48 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public struct ForceData
    {
        public Vector3 vector;
        public ForceDataType type;
    }
}