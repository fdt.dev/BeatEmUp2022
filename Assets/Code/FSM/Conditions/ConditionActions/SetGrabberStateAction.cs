using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   2/15/2022 10:40:45 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public class SetGrabberStateAction : StateActionBase
    {
        [SerializeField] protected string _grabberObjectsState;
        public override void Execute(BEUFighter e)
        {
            if (e.Grabber != null && !string.IsNullOrEmpty(_grabberObjectsState) && e.Grabber is BEUFighter f)
            {
                f.SetState(_grabberObjectsState);
            }
        }
    }
}