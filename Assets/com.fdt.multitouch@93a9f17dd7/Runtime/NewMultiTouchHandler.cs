﻿#if ENABLE_INPUT_SYSTEM

using com.FDT.Common;
using UnityEngine;
using UnityEngine.InputSystem.EnhancedTouch;
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;
using TouchPhase = UnityEngine.TouchPhase;

namespace com.fdt.multitouch
{
    public class NewMultiTouchHandler : MonoBehaviour, IMultiTouchHandler
    {
        [HelpBox("Not working yet"), SerializeField] protected TouchDataUEvt _touchUEvt;
        [SerializeField] protected float _clickTimeTolerance = 0.15f;
        [SerializeField] protected float _longPressTimeTolerance = 0.5f;
        [SerializeField] protected float _moveTolerance = 4f;
        protected int _touches;
        public int touches => _touches;
        public UnityEngine.Object[] occuppied = new UnityEngine.Object[5];
        public TouchData[] touchDatas = new TouchData[5];
        public TouchDataUEvt TouchUEvt => _touchUEvt;
        
        private void OnEnable()
        {
            EnhancedTouchSupport.Enable();
            _touches = 0;
            for (int i = 0; i < 5; i++)
            {
                touchDatas[i] = new TouchData();
                touchDatas[i].fingerId = i;
                touchDatas[i].exists = false;
                touchDatas[i].phase = TouchPhase.Ended;
                touchDatas[i].isDrag = false;
                touchDatas[i].canBeClick = true;
                touchDatas[i].canBeLongPress = true;
                touchDatas[i].moved = false;
            }
        }

        private void Update()
        {
            UpdateTouch();
        }

        protected void UpdateTouch()
        {
            float realtime = Time.realtimeSinceStartup;
            var inputTouches = Touch.activeTouches;
            //int l = Input.touchCount;
            int l = inputTouches.Count;
            for (int i = 0; i < l; i++)
            {
                //var touch = Input.GetTouch(i);
                var touch = inputTouches[i];
                
                //int fingerId = touch.fingerId;
                int fingerId = touch.finger.index;
                if (fingerId > 4)
                    continue;

                Debug.Log($"touch phase {touch.phase}");
                UnityEngine.TouchPhase tp = ParseTouchPhase(touch.phase);
                var touchData = touchDatas[fingerId];
                bool begin = tp == TouchPhase.Began;
                bool ended = tp == TouchPhase.Ended || tp == TouchPhase.Canceled;
                ;
                bool pressing = tp == TouchPhase.Moved || tp == TouchPhase.Stationary;

                touchData.phase = tp;
               // Vector2 fingerPos = touch.position;
                Vector2 fingerPos = touch.screenPosition;
                if (begin)
                {
                    _touches++;
                    touchData.fingerId = fingerId;
                    touchData.phase = TouchPhase.Began;
                    touchData.isDrag = false;
                    touchData.initialRealTime = realtime;
                    touchData.initialPos = fingerPos;
                    touchData.exists = true;
                    touchData.canBeClick = true;
                    touchData.canBeLongPress = true;
                    touchData.moved = false;
                    touchData.lastPos = fingerPos;
                    touchData.currentPos = fingerPos;
                    touchData.deltaPos = Vector2.zero;
                }
                else if (ended)
                {
                    if (touchData.canBeClick)
                    {
                        _touchUEvt.Invoke(TouchEventTypeNum.CLICK, touchData);

                        //_clickUEvt.Invoke(touchData);
                    }
                    else if (touchData.isDrag)
                    {
                        _touchUEvt.Invoke(TouchEventTypeNum.END_DRAG, touchData);
                        //_endDragUEvt.Invoke(touchData);
                    }

                    _touches--;
                    touchData.phase = TouchPhase.Ended;
                    touchData.exists = false;

                }
                else if (pressing)
                {
                    float elapsed = realtime - touchData.initialRealTime;
                    float mag = touchData.deltaPos.magnitude;
                    
                    if (!touchData.isDrag && mag > _moveTolerance)
                    {
                        touchData.moved = true;
                        _touchUEvt.Invoke(TouchEventTypeNum.BEGIN_DRAG, touchData);
                        //_beginDragUEvt.Invoke(touchData);
                        touchData.isDrag = true;
                        touchData.phase = TouchPhase.Moved;
                    }
                    else if (touchData.isDrag && mag > 0)
                    {
                        _touchUEvt.Invoke(TouchEventTypeNum.DRAG, touchData);
                        //_dragUEvt.Invoke(touchData);
                        touchData.phase = TouchPhase.Moved;
                    }
                    else if (mag == 0)
                    {
                        touchData.phase = TouchPhase.Stationary;
                    }
                    
                    if (touchData.moved)
                    {
                        touchData.canBeClick = false;
                        touchData.canBeLongPress = false;
                    }

                    if (touchData.canBeClick && elapsed >= _clickTimeTolerance)
                    {
                        touchData.canBeClick = false;
                    }

                    if (touchData.canBeLongPress && elapsed >= _longPressTimeTolerance)
                    {
                        _touchUEvt.Invoke(TouchEventTypeNum.LONG_PRESS, touchData);
                        //_longPressUEvt.Invoke(touchData);
                        touchData.canBeLongPress = false;
                    }
                }

                touchData.lastPos = touchData.currentPos;
                touchData.currentPos = fingerPos;
                //touchData.deltaPos = touch.deltaPosition;
                touchData.deltaPos = touch.delta;
                touchDatas[fingerId] = touchData;
            }
        }

        private TouchPhase ParseTouchPhase(UnityEngine.InputSystem.TouchPhase touchPhase)
        {
            switch (touchPhase)
            {
                case UnityEngine.InputSystem.TouchPhase.Began:
                    return TouchPhase.Began;
                case UnityEngine.InputSystem.TouchPhase.Canceled:
                    return TouchPhase.Canceled;
                case UnityEngine.InputSystem.TouchPhase.Ended:
                    return TouchPhase.Ended;
                case UnityEngine.InputSystem.TouchPhase.Moved:
                    return TouchPhase.Moved;
                case UnityEngine.InputSystem.TouchPhase.Stationary:
                    return TouchPhase.Stationary;
                case UnityEngine.InputSystem.TouchPhase.None:
                    return TouchPhase.Canceled;
                
            }
            return TouchPhase.Canceled;
        }

        public TouchPhase GetTouchPhase(int fingerId)
        {
            return touchDatas[fingerId].phase;
        }

        public bool HasTouchData(int fingerId)
        {
            return touchDatas[fingerId].exists;
        }

        public TouchData GetData(int fingerId)
        {
            return touchDatas[fingerId];
        }
        public bool IsEditor()
        {
            return Application.isEditor;
        }

        public void SetOccupied(UnityEngine.Object o, int fingerId)
        {
            if (occuppied[fingerId] == null)
            {
                occuppied[fingerId] = o;
            }
            else
            {
                Debug.LogError($"touch {fingerId} occuppied previously by {occuppied[fingerId]}", occuppied[fingerId]);
            }
        }

        public bool IsOccupiedByOther(UnityEngine.Object o, int fingerId)
        {
            return occuppied[fingerId] != null && occuppied[fingerId] != o;
        }

        public void ReleaseOccupied(UnityEngine.Object o, int fingerId)
        {
            if (occuppied[fingerId] == o)
            {
                occuppied[fingerId] = null;
            }
        }

        public int GetOccupiedTouch(UnityEngine.Object o)
        {
            for (int i = 0; i < occuppied.Length; i++)
            {
                if (occuppied[i] == o)
                    return i;
            }
            return -1;
        }
    }
}
#endif