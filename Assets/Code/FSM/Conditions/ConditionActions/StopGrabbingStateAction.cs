using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   2/15/2022 10:40:45 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public class StopGrabbingStateAction : StateActionBase
    {
        [SerializeField] protected string _releasedObjectsState;
        public override void Execute(BEUFighter e)
        {
            foreach (var o in e.GrabbedObjects)
            {
                if (o is BEUFighter f)
                {
                    f.SetState(_releasedObjectsState);
                }
            }
            e.DropGrabbed();
        }
    }
}