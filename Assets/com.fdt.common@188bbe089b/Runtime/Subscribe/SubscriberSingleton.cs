using UnityEngine;

namespace com.FDT.Common.Subscribe
{
    /// <summary>
    /// Creation Date:   3/4/2021 10:20:32 PM
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// Changelog:         
    /// </summary>
    [UnitySingleton(UnitySingletonAttribute.Type.ExistsInScene)]
    public class SubscriberSingleton : Singleton<SubscriberSingleton>, ISubscriber
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        [SerializeField] protected SubscriberData _data = new SubscriberData(); 
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        
        #endregion
                        
        #region Methods
        //[ContextMenu("call from context menu")]
        //public void Test() { }        
        #endregion

        public SubscriberData Data
        {
            get => _data;
        }

        public bool Register(ISubscribable registeredItem)
        {
            return _data.Register(registeredItem);
        }

        public bool Unregister(ISubscribable registeredItem)
        {
            return _data.Unregister(registeredItem);
        }
    }
}