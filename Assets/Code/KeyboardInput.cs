using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace com.FDT.BeatEmUp
{
    /// <summary>
    /// Creation Date:   1/6/2022 8:37:04 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    public class KeyboardInput : MonoBehaviour
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields

        [Header("References"), SerializeField] private InputAsset _inputAsset;
        [SerializeField] private InputActionReference _horizontal;
        [SerializeField] private InputActionReference _vertical;
        #endregion

        #region Properties, Consts and Statics

        #endregion

        #region Variables

        #endregion

        #region Public API

        #endregion

        #region Methods

        private void Update()
        {
            var intention = Vector2.zero;
            intention.x = _horizontal.action.ReadValue<float>();
            intention.y = _vertical.action.ReadValue<float>();
            _inputAsset.intention = intention;
        }

        public void ProcessAttack(InputAction.CallbackContext c)
        {
            _inputAsset.attack = c.performed;
        }
        public void ProcessAttack2(InputAction.CallbackContext c)
        {
            _inputAsset.attack2 = c.performed;
        }
        public void ProcessJump(InputAction.CallbackContext c)
        {
            _inputAsset.jump = c.performed;
        }
        #endregion
    }
}