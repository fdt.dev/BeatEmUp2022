using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.BeatEmUp
{
    /// <summary>
    /// Creation Date:   2/7/2022 10:46:39 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    public class ClampCharacterToCamera : MonoBehaviour
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        [SerializeField] protected float _charEdge = 0; 
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables

        protected bool _playerOutOfScreen = false;
        #endregion

        #region Public API
        
        #endregion
                        
        #region Methods

        private void OnEnable()
        {
            _playerOutOfScreen = true;
        }

        private void Update()
        {
            ClampPlayers();
        }

        private void FixedUpdate()
        {
            ClampPlayers();
        }

        private void LateUpdate()
        {
            ClampPlayers();
        }

        private void ClampPlayers()
        {
            var camera = CameraHandler.Instance;
            
            float minLimit = 0 + _charEdge;
            float maxLimit = 1 - _charEdge;
            if (_playerOutOfScreen)
            {
                var vp = camera.Cam.WorldToViewportPoint(transform.position);
                if (vp.x > minLimit && vp.x < maxLimit)
                {
                    _playerOutOfScreen = false;
                }
            }
            else
            {
                var vp = camera.Cam.WorldToViewportPoint(transform.position);

                if (vp.x < minLimit || vp.x > maxLimit)
                {
                    vp.x = Mathf.Max(minLimit, vp.x);
                    vp.x = Mathf.Min(maxLimit, vp.x);
                    var p = camera.Cam.ViewportToWorldPoint(vp);
                    var np = transform.position;
                    np.x = p.x;
                    transform.position = np;
                }    
            }
            
        }      
        #endregion
    }
}