namespace com.FDT.Common
{
    /// <summary>
    /// Creation Date:   7/14/2020 11:13:29 PM
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public interface IObjectType
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("GameEvents"), SerializeField] 
        //[Header("UnityEvents"), SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("Public"), SerializeField] 
        //[Header("Protected"), SerializeField]
        //[Header("Private"), SerializeField]
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        #endregion
                        
        #region Methods
                
        #endregion
    }
}