﻿using UnityEditor;

namespace com.FDT.Common.Editor
{
    /// <summary>
    /// Creation Date:   23/02/2020 20:41:25
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
	public class CastedDecoratorDrawer<T> : DecoratorDrawer where T:PropertyAttributeBase
	{
        #region Properties, Consts and Statics
		protected T cAttribute => (T)attribute;
        #endregion
    }
}