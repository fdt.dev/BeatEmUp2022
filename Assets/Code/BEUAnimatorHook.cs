using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.BeatEmUp
{
    /// <summary>
    /// Creation Date:   1/4/2022 12:33:25 AM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     This handles the information from the root motion
    /// Changelog:         
    /// </summary>
    public class BEUAnimatorHook : MonoBehaviour
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        [SerializeField] protected Animator _anim;
        [SerializeField] protected Vector3 _delta;


        #endregion

        #region Properties, Consts and Statics
        public Vector3 Delta => _delta;
        public bool HasDelta = false;
        #endregion

        #region Variables

        #endregion

        #region Public API
        public void Play(string a)
        {
            _anim.Play(a, 0, 0);
        }

        #endregion

        #region Methods

        private void OnAnimatorMove()
        {
            _delta = _anim.deltaPosition;
            HasDelta = true;
        }

        #endregion


    }
}