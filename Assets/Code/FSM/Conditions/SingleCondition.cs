using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   1/3/2022 4:21:13 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public class SingleCondition : BaseConditionType
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields

        [Juce.ImplementationSelector.SelectImplementation(typeof(IStateCondition), false, true)]
        [SerializeField, SerializeReference]
        protected IStateCondition _condition;

        #endregion

        #region Properties, Consts and Statics

        #endregion

        #region Variables

        #endregion

        #region Public API
        public override bool CheckConditions(BEUFighter e)
        {
            if (_condition.CheckCondition(e))
            {
                return true;
            }
            return false;
        }
        #endregion

        #region Methods
        #endregion
    }
}