using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.BeatEmUp
{
    /// <summary>
    /// Creation Date:   1/11/2022 11:12:58 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    public class Hitbox : MonoBehaviour
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        [SerializeField] protected BEUObject _owner; 
        [SerializeField] private Collider _collider;
        [SerializeField] protected LayerMask _attackMask;
        
        #endregion

        #region Properties, Consts and Statics
        
        protected static int attacksCurrentID = 0;
        public BEUObject Owner => _owner;
        public int HitStateID => _hitStateID;

        protected int _hitStateID;

        private List<BEUObject> _collisions = new List<BEUObject>();

        #endregion

        #region Variables
        
        #endregion

        #region Public API
        
        #endregion
                        
        #region Methods

        private void OnEnable()
        {
            attacksCurrentID++;
            _hitStateID = attacksCurrentID;
        }

        private void OnTriggerEnter(Collider other)
        {
            var o = BoundingBox.GetOwner(other);
            if (o != null && o != _owner && o.LastAttackReceived != _hitStateID)
            {
                _collisions.Add(o);
            }
        }
        private void OnTriggerExit(Collider other)
        {
            Debug.Log($"OnTriggerExit from {_owner.name} hitbox to {other.name}");
            var o = BoundingBox.GetOwner(other);
            if (o != null && _collisions.Contains(o))
            {
                _collisions.Remove(o);
            }
        }

        private void OnDisable()
        {
            _collisions.Clear();
        }
        
        public BEUObject CheckCollision()
        {
            if (_collider.gameObject.activeInHierarchy)
            {
                if (_collisions.Count > 0)
                    return _collisions[0];

            }
            return null;
        }
        #endregion
    }
}