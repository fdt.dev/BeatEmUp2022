﻿#if !ENABLE_INPUT_SYSTEM
using UnityEngine;
using UnityEngine.EventSystems;

namespace com.fdt.multitouch
{
    [RequireComponent(typeof(EventSystem))]
    public class MultiTouchHandler : MonoBehaviour, IMultiTouchHandler
    {
        [SerializeField] protected TouchDataUEvt _touchUEvt;
        [SerializeField] protected float _clickTimeTolerance = 0.15f;
        [SerializeField] protected float _longPressTimeTolerance = 0.5f;
        [SerializeField] protected float _moveTolerance = 4f;
        protected int _touches;
        public int touches => _touches;
        public UnityEngine.Object[] occuppied = new UnityEngine.Object[5];
        public TouchData[] touchDatas = new TouchData[5];
        public TouchDataUEvt TouchUEvt => _touchUEvt;
        
        private void OnEnable()
        {
            #if ENABLE_INPUT_SYSTEM && ENABLE_LEGACY_INPUT_MANAGER
            Debug.Log("both");
            #elif ENABLE_INPUT_SYSTEM
            Debug.Log("new input system");
            #elif ENABLE_LEGACY_INPUT_MANAGER
            Debug.Log("legacy input manager");
            #endif

            _touches = 0;
            for (int i = 0; i < 5; i++)
            {
                touchDatas[i] = new TouchData();
                touchDatas[i].fingerId = i;
                touchDatas[i].exists = false;
                touchDatas[i].phase = TouchPhase.Ended;
                touchDatas[i].isDrag = false;
                touchDatas[i].canBeClick = true;
                touchDatas[i].canBeLongPress = true;
                touchDatas[i].moved = false;
            }
        }

        private void Update()
        {
            bool useTouch = !Application.isEditor;
#if UNITY_EDITOR
            if (!useTouch && UnityEditor.EditorApplication.isRemoteConnected)
            {
                useTouch = true;
            }
#endif

            if (!useTouch)
            {
                UpdateMouse();
            }
            else
            {
                UpdateTouch();
            }
        }

        private void LateUpdate()
        {
            bool useTouch = !Application.isEditor;
#if UNITY_EDITOR
            if (!useTouch && UnityEditor.EditorApplication.isRemoteConnected)
            {
                useTouch = true;
            }
#endif

            if (!useTouch)
            {
                UpdateMouse();
            }
            else
            {
                UpdateTouch();
            }
        }
        private void UpdateMouse()
        {
            float realtime = Time.realtimeSinceStartup;
            for (int i = 0; i < 2; i++)
            {
                var touchData = touchDatas[i];
                bool begin = Input.GetMouseButtonDown(i);
                bool ended = Input.GetMouseButtonUp(i);
                bool pressing = Input.GetMouseButton(i);
                Vector2 mPos = Input.mousePosition;
                if (begin)
                {
                    _touches++;
                    touchData.fingerId = i;
                    touchData.phase = TouchPhase.Began;
                    touchData.isDrag = false;
                    touchData.initialRealTime = realtime;
                    touchData.initialPos = mPos;
                    touchData.exists = true;
                    touchData.canBeClick = true;
                    touchData.canBeLongPress = true;
                    touchData.moved = false;
                    touchData.lastPos = mPos;
                    touchData.currentPos = mPos;
                    touchData.deltaPos = Vector2.zero;
                    _touchUEvt.Invoke(TouchEventTypeNum.PRESSED, touchData);
                }
                else if (ended)
                {
                    if (touchData.canBeClick)
                    {
                        _touchUEvt.Invoke(TouchEventTypeNum.CLICK, touchData);
                        //_clickUEvt.Invoke(touchData);
                    }
                    else if (touchData.isDrag)
                    {
                        _touchUEvt.Invoke(TouchEventTypeNum.END_DRAG, touchData);
                        //_endDragUEvt.Invoke(touchData);
                    }

                    touchData.phase = TouchPhase.Ended;
                    _touchUEvt.Invoke(TouchEventTypeNum.END_PRESS, touchData);
                    _touches--;
                    touchData.exists = false;
                }
                else if (pressing)
                {
                    float elapsed = realtime - touchData.initialRealTime;
                    float mag = touchData.deltaPos.magnitude;
                    
                    if (!touchData.isDrag && mag > _moveTolerance)
                    {
                        touchData.moved = true;
                        _touchUEvt.Invoke(TouchEventTypeNum.BEGIN_DRAG, touchData);
                        //_beginDragUEvt.Invoke(touchData);
                        touchData.isDrag = true;
                        touchData.phase = TouchPhase.Moved;
                    }
                    else if (touchData.isDrag && mag > 0)
                    {
                        _touchUEvt.Invoke(TouchEventTypeNum.DRAG, touchData);
                        //_dragUEvt.Invoke(touchData);
                        touchData.phase = TouchPhase.Moved;
                    }
                    else if (mag == 0)
                    {
                        touchData.phase = TouchPhase.Stationary;
                    }

                    if (touchData.moved)
                    {
                        touchData.canBeClick = false;
                        touchData.canBeLongPress = false;
                    }

                    if (touchData.canBeClick && elapsed >= _clickTimeTolerance)
                    {
                        touchData.canBeClick = false;
                    }

                    if (touchData.canBeLongPress && elapsed >= _longPressTimeTolerance)
                    {
                        _touchUEvt.Invoke(TouchEventTypeNum.LONG_PRESS, touchData);
                        //_longPressUEvt.Invoke(touchData);
                        touchData.canBeLongPress = false;
                    }
                }

                touchData.lastPos = touchData.currentPos;
                touchData.currentPos = mPos;
                touchData.deltaPos = touchData.currentPos - touchData.lastPos;
                touchDatas[i] = touchData;
            }
        }

        protected void UpdateTouch()
        {
            float realtime = Time.realtimeSinceStartup;
            int l = Input.touchCount;
            for (int i = 0; i < l; i++)
            {
                var touch = Input.GetTouch(i);
                int fingerId = touch.fingerId;
                if (fingerId > 4)
                    continue;

                var touchData = touchDatas[fingerId];
                bool begin = touch.phase == TouchPhase.Began;
                bool ended = touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled;
                ;
                bool pressing = touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary;

                touchData.phase = touch.phase;
                Vector2 fingerPos = touch.position;
                if (begin)
                {
                    _touches++;
                    touchData.fingerId = fingerId;
                    touchData.phase = TouchPhase.Began;
                    touchData.isDrag = false;
                    touchData.initialRealTime = realtime;
                    touchData.initialPos = fingerPos;
                    touchData.exists = true;
                    touchData.canBeClick = true;
                    touchData.canBeLongPress = true;
                    touchData.moved = false;
                    touchData.lastPos = fingerPos;
                    touchData.currentPos = fingerPos;
                    touchData.deltaPos = Vector2.zero;
                    _touchUEvt.Invoke(TouchEventTypeNum.PRESSED, touchData);
                }
                else if (ended)
                {
                    if (touchData.canBeClick)
                    {
                        _touchUEvt.Invoke(TouchEventTypeNum.CLICK, touchData);

                        //_clickUEvt.Invoke(touchData);
                    }
                    else if (touchData.isDrag)
                    {
                        _touchUEvt.Invoke(TouchEventTypeNum.END_DRAG, touchData);
                        //_endDragUEvt.Invoke(touchData);
                    }
                    
                    touchData.phase = TouchPhase.Ended;
                    _touchUEvt.Invoke(TouchEventTypeNum.END_PRESS, touchData);
                    _touches--;
                    touchData.exists = false;

                }
                else if (pressing)
                {
                    float elapsed = realtime - touchData.initialRealTime;
                    float mag = touchData.deltaPos.magnitude;
                    
                    if (!touchData.isDrag && mag > _moveTolerance)
                    {
                        touchData.moved = true;
                        _touchUEvt.Invoke(TouchEventTypeNum.BEGIN_DRAG, touchData);
                        //_beginDragUEvt.Invoke(touchData);
                        touchData.isDrag = true;
                        touchData.phase = TouchPhase.Moved;
                    }
                    else if (touchData.isDrag && mag > 0)
                    {
                        _touchUEvt.Invoke(TouchEventTypeNum.DRAG, touchData);
                        //_dragUEvt.Invoke(touchData);
                        touchData.phase = TouchPhase.Moved;
                    }
                    else if (mag == 0)
                    {
                        touchData.phase = TouchPhase.Stationary;
                    }
                    
                    if (touchData.moved)
                    {
                        touchData.canBeClick = false;
                        touchData.canBeLongPress = false;
                    }

                    if (touchData.canBeClick && elapsed >= _clickTimeTolerance)
                    {
                        touchData.canBeClick = false;
                    }

                    if (touchData.canBeLongPress && elapsed >= _longPressTimeTolerance)
                    {
                        _touchUEvt.Invoke(TouchEventTypeNum.LONG_PRESS, touchData);
                        //_longPressUEvt.Invoke(touchData);
                        touchData.canBeLongPress = false;
                    }
                }

                touchData.lastPos = touchData.currentPos;
                touchData.currentPos = fingerPos;
                touchData.deltaPos = touch.deltaPosition;
                touchDatas[fingerId] = touchData;
            }
        }

        public TouchPhase GetTouchPhase(int fingerId)
        {
            return touchDatas[fingerId].phase;
        }

        public bool HasTouchData(int fingerId)
        {
            return touchDatas[fingerId].exists;
        }

        public TouchData GetData(int fingerId)
        {
            return touchDatas[fingerId];
        }
        public bool IsEditor()
        {
            return Application.isEditor;
        }

        public void SetOccupied(UnityEngine.Object o, int fingerId)
        {
            if (occuppied[fingerId] == null)
            {
                occuppied[fingerId] = o;
            }
            else
            {
                Debug.LogError($"touch {fingerId} occuppied previously by {occuppied[fingerId]}", occuppied[fingerId]);
            }
        }

        public bool IsOccupiedByOther(UnityEngine.Object o, int fingerId)
        {
            return occuppied[fingerId] != null && occuppied[fingerId] != o;
        }

        public void ReleaseOccupied(UnityEngine.Object o, int fingerId)
        {
            if (occuppied[fingerId] == o)
            {
                occuppied[fingerId] = null;
            }
        }

        public int GetOccupiedTouch(UnityEngine.Object o)
        {
            for (int i = 0; i < occuppied.Length; i++)
            {
                if (occuppied[i] == o)
                    return i;
            }
            return -1;
        }
    }
}
#endif