﻿public interface ISingleton
{
    /// <summary>
    /// Creation Date:   
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    void OnDynamicCreation();
}
