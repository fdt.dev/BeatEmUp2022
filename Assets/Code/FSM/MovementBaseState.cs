using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   1/2/2022 11:20:32 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public abstract class MovementBaseState : IStateType
    { 
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields

        //[SerializeField] protected float _runSpeed = 1;
        
        #endregion

        #region Properties, Consts and Statics

        #endregion

        #region Variables

        #endregion

        #region Public API

        public virtual void InitState(BEUFighter e)
        {
            
        }

        public virtual void ExecuteState(BEUFighter e, BeatEmUpState state)
        {
            
        }

        public virtual bool HitBoxCollided(Hitbox myHitbox, BEUFighter e, BEUObject target)
        {
            return false;
        }
        public virtual void CollidedWithHitbox(Hitbox myHitbox, BEUFighter e, BEUObject attacker)
        {
            e.ModHealth(-myHitbox.Owner.AttackDamage);
            e.SetLastReceivedAttack(myHitbox);
        }
        #endregion

        #region Methods

        //[ContextMenu("call from context menu")]
        //public void Test() { }        
        #endregion
    }
}