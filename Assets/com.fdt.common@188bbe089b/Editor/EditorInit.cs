﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;

namespace com.FDT.Common.Editor
{
    /// <summary>
    /// Creation Date:   23/02/2020 20:51:10
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     Adds the given define symbols to PlayerSettings define symbols.
    ///                  Just add your own define symbols to the Symbols property at the below.
    /// </summary>
    public class EditorInit : UnityEditor.Editor
    {
        protected static bool changedSettings = false;
        #region Methods
        [UnityEditor.Callbacks.DidReloadScripts]
        public static void Init()
        {
            changedSettings = false;
            FindPackage("FDTInitializationHandler t:asmdef", "INITHANDLER");
            FindPackage("FDTGenericDataSaving t:asmdef", "GENERICDATASAVING");
            FindPackage("FDTStateMachineGraph t:asmdef", "SMGRAPH");
            if (changedSettings)
            {
                UnityEngine.Debug.Log("Running com.FDT.Common.Editor.EditorInit - settings changed");
            }
        }
        private static void FindPackage(string asmdef, string param)
        {
            string definesString =
                PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
            List<string> allDefines = definesString.Split(';').ToList();

            var o = AssetDatabase.FindAssets(asmdef);
            bool mod = false;
            if (o.Length > 0 && !allDefines.Contains(param))
            {
                allDefines.Add(param);
                mod = true;
            }
            else if (o.Length == 0 && allDefines.Contains(param))
            {
                allDefines.Remove(param);
                mod = true;
            }

            if (mod)
            {
                changedSettings = true;
                PlayerSettings.SetScriptingDefineSymbolsForGroup(
                    EditorUserBuildSettings.selectedBuildTargetGroup,
                    string.Join(";", allDefines.ToArray()));
            }
        }
        #endregion
    }
}
