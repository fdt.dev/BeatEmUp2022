using System.Collections;
using System.Collections.Generic;
using com.FDT.BeatEmUp;
using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   1/3/2022 4:39:17 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [Juce.ImplementationSelector.SelectImplementationCustomDisplayName("Input - Attack2")]
    public class InputAttack2Condition : ConditionBase
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        
        #endregion
                        
        #region Methods
        public override bool CheckCondition(BEUFighter beuFighter)
        {
            bool attack2Requested = beuFighter.Attack2Requested;
            if (attack2Requested)
            {
                return true;
            }
            return false;
        }         
        #endregion
    }
}