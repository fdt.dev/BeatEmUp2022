using System.Collections;
using System.Collections.Generic;
using com.FDT.BeatEmUp;
using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   1/3/2022 4:24:13 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public abstract class BaseConditionType : IStateConditionType
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        [SerializeField] protected string _targetState;
        
        [Juce.ImplementationSelector.SelectImplementation(typeof(IStateAction), false, true)]
        [SerializeField, SerializeReference]
        protected List<IStateAction> _actions = new List<IStateAction>();
        
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        
        #endregion
                        
        #region Methods
        //[ContextMenu("call from context menu")]
        //public void Test() { }        
        #endregion

        public virtual bool CheckConditions(BEUFighter e)
        {
            return false;
        }

        public void ExecuteConditionActions(BEUFighter beuFighter)
        {
            for (int i = 0; i < _actions.Count; i++)
            {
                _actions[i].Execute(beuFighter);
            }
        }

        public void ExecuteConditionSuccess(BEUFighter beuFighter)
        {
            beuFighter.SetState(_targetState);
        }
    }
}