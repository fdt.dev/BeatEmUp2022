using UnityEngine;

namespace com.FDT.Common.Example
{
    /// <summary>
    /// Creation Date:   
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public class TestableAdaptor : MonoBehaviour, ITestable
    {
        public Testable adapted;
        public int a => adapted.a;
    }
}