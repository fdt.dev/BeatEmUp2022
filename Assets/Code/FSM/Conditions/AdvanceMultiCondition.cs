using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   1/3/2022 4:21:02 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    [Juce.ImplementationSelector.SelectImplementationCustomDisplayName("Adv Multi Condition")]
    public class AdvanceMultiCondition : IStateConditionType
    {
        #region Classes, Structs and Enums

        public enum ConditionType
        {
            AND, OR
        }
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields

        [SerializeField] protected ConditionType _conditionType;
        [Juce.ImplementationSelector.SelectImplementation(typeof(IStateCondition), false, true)]
        [SerializeField, SerializeReference] protected List<IStateCondition> _conditions = new List<IStateCondition>();

        [Juce.ImplementationSelector.SelectImplementation(typeof(IStateAction), false, true)]
        [SerializeField, SerializeReference]
        protected List<IStateAction> _actions = new List<IStateAction>();
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        public virtual bool CheckConditions(BEUFighter e)
        {
            switch (_conditionType)
            {
                case ConditionType.AND:
                    for (int i = 0; i < _conditions.Count; i++)
                    {
                        var c = _conditions[i];
                        var r = c.CheckCondition(e);
                        if (!r)
                        {
                            return false;
                        }
                    }
                    return true;
                    break;
                case ConditionType.OR:
                    for (int i = 0; i < _conditions.Count; i++)
                    {
                        var c = _conditions[i];
                        var r = c.CheckCondition(e);
                        if (r)
                        {
                            return true;
                        }
                    }
                    return false;
                    break;
            }
            return false;
        }

        public void ExecuteConditionActions(BEUFighter beuFighter)
        {
            for (int i = 0; i < _actions.Count; i++)
            {
                _actions[i].Execute(beuFighter);
            }
        }

        public void ExecuteConditionSuccess(BEUFighter beuFighter)
        {
            
        }

        #endregion
                        
        #region Methods

        #endregion
    }
}