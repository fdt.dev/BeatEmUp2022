namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   1/3/2022 4:02:30 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [Juce.ImplementationSelector.SelectImplementationDefaultType]
    public interface IStateType
    {
        void InitState(BEUFighter e);
        void ExecuteState(BEUFighter e, BeatEmUpState state);
        bool HitBoxCollided(Hitbox myHitbox, BEUFighter e, BEUObject target);
        void CollidedWithHitbox(Hitbox myHitbox, BEUFighter e, BEUObject attacker);
    }
}