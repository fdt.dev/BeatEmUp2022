using System.Collections;
using System.Collections.Generic;
using com.FDT.BeatEmUp.FSM;
using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   2/17/2022 9:40:55 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public abstract class StateAnimationBase : IStateAnimation
    {
        public virtual void Execute(BEUAnimatorHook anim)
        {
            
        }
    }
}