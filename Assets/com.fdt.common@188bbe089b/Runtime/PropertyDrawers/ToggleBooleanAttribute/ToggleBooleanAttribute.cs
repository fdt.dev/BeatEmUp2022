﻿using System;

namespace com.FDT.Common
{
    /// <summary>
    /// Creation Date:   01/02/2020 22:51:22
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
	[AttributeUsage(AttributeTargets.Field, Inherited = true)]
	public class ToggleBooleanAttribute : PropertyAttributeBase 
	{
		
	}
}