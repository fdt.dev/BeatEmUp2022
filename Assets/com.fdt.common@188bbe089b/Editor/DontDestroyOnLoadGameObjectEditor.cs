﻿using UnityEditor;

namespace com.FDT.Common.Editor
{
    /// <summary>
    /// Creation Date:   23/02/2020 20:48:35
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// </summary>
    [CustomEditor(typeof(DontDestroyOnLoadGameObject))]
    public class DontDestroyOnLoadGameObjectEditor : UnityEditor.Editor
    {
        #region Methods
        public override void OnInspectorGUI()
        {
            EditorGUILayout.HelpBox("This GameObject will not be destroyed when loading scene", MessageType.Info);
            DrawDefaultInspector();
        }
        #endregion
    }
}