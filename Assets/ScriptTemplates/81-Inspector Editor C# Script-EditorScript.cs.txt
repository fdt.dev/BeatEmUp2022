using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace #NAMESPACE#
{
    /// <summary>
    /// Creation Date:   #CREATIONDATE#
    /// Product Name:    #PROJECTNAME#
    /// Developers:      #DEV#
    /// Company:         #COMPANY#
    /// Description:     #NOTRIM#
    /// Changelog:       #NOTRIM#  
    /// </summary>
    [CustomEditor(typeof(object), true)]
    public class #SCRIPTNAME# : UnityEditor.Editor
    {
        #region Classes, Structs and Enums
        #NOTRIM#
        #endregion

        #region Properties, Consts and Statics
        #NOTRIM#
        #endregion
        
        #region Editor variables
        protected object cTarget;
        #endregion

        #region Methods
        private void OnEnable()
        {
            // initialization
            cTarget = target as object;
        }
        public override void OnInspectorGUI()
        {            
            serializedObject.Update();
            DrawDefaultInspector();
            serializedObject.ApplyModifiedProperties();            
        }        
        #endregion
    }
}