namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   1/3/2022 4:21:02 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    [Juce.ImplementationSelector.SelectImplementationCustomDisplayName("Multi Condition OR")]
    public class OrMultiCondition : MultiConditionBase
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields

        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        public override bool CheckConditions(BEUFighter e)
        {
            for (int i = 0; i < _conditions.Count; i++)
            {
                var c = _conditions[i];
                var r = c.CheckCondition(e);
                if (r)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion
                        
        #region Methods

        #endregion
    }
}