using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   2/9/2022 7:28:57 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public class GrabStateAction : StateActionBase
    {
        [SerializeField] protected string _grabbedState;
        public override void Execute(BEUFighter e)
        {
            var o = e.HitBox.CheckCollision();
            if (o != null)
            {
                e.Grab(o);
                o.SetGrabbedBy(e);
                if (o is BEUFighter f)
                {
                    f.SetState(_grabbedState);
                }
            }
                
        }
    }
}