﻿using System.Collections.Generic;
using com.FDT.Common.ReloadedScriptableObject;
using UnityEngine;

namespace com.FDT.Common.Registrables
{
    /// <summary>
    /// Creation Date:   28/02/2020 13:26:05
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public abstract class SingletonRegisterer<TSingletonClass, TRegistrable, TRegistrableID> :
        Singleton<TSingletonClass>, IResetOnPlay, IRegisterer<TRegistrable, TRegistrableID> 
        where TRegistrableID : IRegistrableID
        where TRegistrable : IRegistrable<TRegistrableID>
        where TSingletonClass : MonoBehaviour, ISingleton
    {
        
        #region Properties, Consts and Statics
        public List<TRegistrable> Registered
        {
            get { return _registered; }
        }
        public int Count
        {
            get { return _count; }
        }
        public TRegistrable this[int idx] => (idx < _registered.Count) ? _registered[idx] : default;
        public TRegistrable this[TRegistrableID id] =>
            (_registeredByID.ContainsKey(id)) ? _registeredByID[id] : default;
        #endregion

        #region Variables
        protected Dictionary<TRegistrableID, TRegistrable> _registeredByID =
            new Dictionary<TRegistrableID, TRegistrable>();
        protected List<TRegistrable> _registered = new List<TRegistrable>();
        protected int _count = 0;
        #endregion
        
        #region Public API
        public bool IsType<T>(TRegistrableID registeredID) where T : TRegistrable
        {
            return _registeredByID[registeredID] is T;
        }
        public T GetInstance<T>(TRegistrableID registeredID) where T : class, TRegistrable
        {
            return _registeredByID[registeredID] as T;
        }
        public bool ItemExists(TRegistrableID registeredID)
        {
            return _registeredByID.ContainsKey(registeredID);
        }
        public virtual bool Register(TRegistrable registeredItem)
        {
            if (registeredItem.referenceID == null || _registeredByID.ContainsKey(registeredItem.referenceID)) return false;
            _registeredByID.Add(registeredItem.referenceID, registeredItem);
            _registered.Add(registeredItem);
            _count++;
            ReactToRegistration(registeredItem);
            return true;
        }
        public bool Unregister(TRegistrable registeredItem)
        {
            if (!_registeredByID.ContainsKey(registeredItem.referenceID)) return false;
            ReactToUnregistration(registeredItem);
            if (registeredItem.referenceID != null)
                _registeredByID.Remove(registeredItem.referenceID);
            _registered.Remove(registeredItem);
            _count--;
            return true;
        }

        public void ResetValues()
        {            
            _registered.Clear();
            _registeredByID.Clear();
            _count = 0;
        }
        #endregion

        #region Methods
        protected virtual void ReactToRegistration(TRegistrable registeredItem) { }
        protected virtual void ReactToUnregistration(TRegistrable registeredItem) { }
        #endregion

    }
}
