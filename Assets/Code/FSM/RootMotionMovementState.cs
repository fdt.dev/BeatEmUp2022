using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   1/3/2022 6:37:19 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     Common state that doesn't use vectors to move, and instead it uses
    ///                  rootmotion from the animations
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public class RootMotionMovementState : MovementBaseState
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        public override void InitState(BEUFighter e)
        {
            e.rb.IsKinematic = true;
            e.rb.SetTargetVelocity(Vector3.zero);
        }

        public override void ExecuteState(BEUFighter e, BeatEmUpState state)
        {
            if (e.Anim.HasDelta)
            {
                Vector3 delta = e.Anim.Delta;
                delta.x *= e.facingDirection;
                e.rb.ProcessVelocity(delta);
                e.Anim.HasDelta = false;
            }
        }
        #endregion
                        
        #region Methods
        //[ContextMenu("call from context menu")]
        //public void Test() { }        
        #endregion
    }
}