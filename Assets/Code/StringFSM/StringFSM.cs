using System.Collections.Generic;
using Juce.ImplementationSelector;
using UnityEngine;

namespace StringFSMSystem
{
    [System.Serializable]
    public class StringFSM<T2>:StringFSMBase where T2: BaseFSMState
    {
        [SelectImplementationCustomDisplayName("IState")]
        [SerializeField]
        protected List<T2> States = new List<T2>();
        
        [SerializeField] protected string currentState;
        protected Dictionary<string, T2> cache = new Dictionary<string, T2>();
        public string CurrentState => currentState;

        public void InitFSM()
        {
            SetState(currentState);
        }
        public void SetState(string state)
        {
            currentState = state;
        }

        public T2 GetCurrentStateData()
        {
            return GetStateData(currentState);
        }
        public T2 GetStateData(string state)
        {
            if (cache.Count == 0)
            {
                foreach (var sd in States)
                {
                    cache.Add(sd.StateID, sd);
                }
            }
            #if UNITY_EDITOR
            if (!cache.ContainsKey(state))
                Debug.LogError($"{state} is not found in cache.");
            #endif
            return cache[state];
        }

        public bool HasState(string s)
        {
            return cache.ContainsKey(s);
        }
    }
}
