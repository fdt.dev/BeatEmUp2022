using System;
using UnityEngine;

namespace com.FDT.BeatEmUp
{
    /// <summary>
    /// Creation Date:   1/2/2022 8:27:14 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     This will be refactored later. It changes the position of the sprite for the game view,
    ///                  but keep it in the Scene view. This makes the game looks effectively in 2d while the
    ///                  gameplay is 3D 
    /// Changelog:         
    /// </summary>
    [ExecuteAlways]
    public class TestVisibility : MonoBehaviour
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        [SerializeField] protected Transform _handle;
        [SerializeField] protected float _offsetY = 0;
        [SerializeField] protected Transform _main;
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        
        #endregion
                        
        #region Methods

        private void Update()
        {
            _handle.localPosition = Vector3.zero;
        }

        private void LateUpdate()
        {
            _handle.localPosition = Vector3.zero;
        }

        void OnWillRenderObject()
        {
            if (Camera.current == null || _handle == null || _main == null)
            {
                _handle.localPosition = Vector3.zero;
                return;
            }

            if (Camera.current.name == "SceneCamera")
            {
                _handle.localPosition = Vector3.zero;
            }
            else
            {
                var v = _handle.localPosition;
                v.y = _main.position.z - _offsetY;
                _handle.localPosition = v;
            }
        }
        //[ContextMenu("call from context menu")]
        //public void Test() { }        
        #endregion
    }
}