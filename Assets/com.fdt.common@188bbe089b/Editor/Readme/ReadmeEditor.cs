using UnityEditor;
using UnityEngine;

namespace com.FDT.Common.Editor
{
    /// <summary>
    /// Creation Date:   4/14/2020 12:19:09 PM
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// </summary>
    [CustomEditor(typeof(Readme))]
    public class ReadmeEditor : UnityEditor.Editor
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("GameEvents"), SerializeField] 
        //[Header("UnityEvents"), SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("Public"), SerializeField] 
        //[Header("Protected"), SerializeField]
        //[Header("Private"), SerializeField]
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables

        protected SerializedProperty colorProp;
        #endregion

        #region Public API
        #endregion
                        
        #region Methods

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            colorProp = serializedObject.FindProperty("_color");
            Color oldColor = GUI.backgroundColor;
            GUI.color = colorProp.colorValue;
            //GUI.Box(new Rect(0, 0, 10, 10), GUIContent.none );
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_description"), true);
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(colorProp, true);
            if (EditorGUI.EndChangeCheck())
            {
                Color c = colorProp.colorValue;
                c.a = Mathf.Min(c.a, 0.75f);
                colorProp.colorValue = c;
            }

            GUI.backgroundColor = oldColor;
            serializedObject.ApplyModifiedProperties();
        }

        #endregion
    }
}