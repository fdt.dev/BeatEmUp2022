
namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   2/17/2022 9:41:14 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [Juce.ImplementationSelector.SelectImplementationDefaultType]
    public interface IStateAnimation
    {
        void Execute(BEUAnimatorHook anim);
    }
}