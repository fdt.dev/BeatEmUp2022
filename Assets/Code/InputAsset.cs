using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.BeatEmUp
{
    /// <summary>
    /// Creation Date:   4/25/2021 9:36:01 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     Offers a common place to put player input and AI
    /// Changelog:         
    /// </summary>
    [CreateAssetMenu(menuName = "BeatEmUp/InputAsset", fileName = "New InputAsset")]
    public class InputAsset : ScriptableObject, IInput
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields

        public bool _attack = false;
        public bool _attack2 = false;
        public bool _jump = false;
        
        [SerializeField] protected Vector2 _intention;
        [SerializeField] protected float _deadZone = 0.2f;
        
        #endregion

        #region Properties, Consts and Statics
        public static float DEADZONE = 0.1f;
        public Vector2 intention
        {
            get
            {
                if (_intention.magnitude > _deadZone)
                {
                    return _intention;    
                }
                return Vector2.zero;
            } 
            set => _intention = value;
        }

        public bool attack { get => _attack; set => _attack = value; }
        public bool attack2 { get => _attack2; set => _attack2 = value; }
        public bool jump { get => _jump; set => _jump = value; }

        public int aiRequestedIdx { get; set; }
        #endregion

        #region Variables
        
        #endregion

        #region Public API

        #endregion

        #region Methods

        #endregion
    }
}