using System.Collections;
using System.Collections.Generic;
using com.FDT.BeatEmUp;
using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   1/3/2022 4:48:49 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public abstract class ConditionBase : IStateCondition
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        #endregion

        #region Properties, Consts and Statics

        #endregion

        #region Variables

        #endregion

        #region Public API

        #endregion

        #region Methods

        //[ContextMenu("call from context menu")]
        //public void Test() { }        

        #endregion

        public virtual bool CheckCondition(BEUFighter beuFighter)
        {
            return false;
        }
    }
}