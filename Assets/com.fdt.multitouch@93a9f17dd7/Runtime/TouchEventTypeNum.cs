using UnityEngine;

namespace com.fdt.multitouch
{
    // --------------
    // Creation Date:   8/8/2020 3:12:43 PM
    // Product Name:    Game
    // Developers:      FDT Dev
    // Company:         FDT Dev
    // Description:     
    // Changelog:       
    // --------------
    public enum TouchEventTypeNum
    {
        NONE = 0, PRESSED = 1, CLICK = 2, LONG_PRESS = 3, BEGIN_DRAG = 4, DRAG = 5, END_DRAG = 6, END_PRESS = 7
    }
}