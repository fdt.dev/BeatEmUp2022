using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   2/15/2022 11:37:55 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public class FaceAttackerStateAction : StateActionBase
    {
        public override void Execute(BEUFighter e)
        {
            if (e.HitData?.Owner != null)
                e.FacePoint(e.HitData.Owner.transform.position);
        }
    }
}