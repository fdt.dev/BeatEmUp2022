﻿using UnityEngine.Events;

namespace com.fdt.multitouch
{
    [System.Serializable]
    public class TouchDataUEvt : UnityEvent<TouchEventTypeNum, TouchData>
    {

    }
}