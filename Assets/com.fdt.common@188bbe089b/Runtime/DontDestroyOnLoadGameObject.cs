﻿using UnityEngine;

namespace com.FDT.Common
{
    /// <summary>
    /// Creation Date:   
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     Adds DontDestroyOnLoad to a gameObject
    /// </summary>
    public class DontDestroyOnLoadGameObject : MonoBehaviour
    {
        #region Methods
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }
        #endregion
    }
}