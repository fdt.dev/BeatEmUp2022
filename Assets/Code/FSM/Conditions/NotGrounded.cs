namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   1/4/2022 9:03:58 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [Juce.ImplementationSelector.SelectImplementationCustomDisplayName("NotGrounded")]
    public class NotGrounded : ConditionBase
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        
        #endregion
                        
        #region Methods
        public override bool CheckCondition(BEUFighter beuFighter)
        {
            return !beuFighter.rb.IsGrounded;
        }     
        #endregion
    }
}