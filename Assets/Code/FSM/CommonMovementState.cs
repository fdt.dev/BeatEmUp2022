using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   1/3/2022 6:37:19 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     Common State that doesn't use rootmotion, and uses movement vectors instead
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public class CommonMovementState : MovementBaseState
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        [SerializeField] protected float _gravityMultiplier;

        //[Header("")]
        [SerializeField] protected bool _canInputMove;
        [SerializeField] protected bool _clearReceivedAttack;
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        public override void InitState(BEUFighter e)
        {
            if (_clearReceivedAttack)
            {
                e.consumedAttack = true;
            }
            e.rb.gravityModifier = _gravityMultiplier;
            e.rb.IsKinematic = false;
        }  
        public override void ExecuteState(BEUFighter e, BeatEmUpState state)
        {
            if (_canInputMove)
            {
                Vector2 velocity = new Vector2(e.Intention.x * state.RunSpeed, e.Intention.y * state.RunSpeed);
                e.SetMovementSpeed(velocity);
            }
        }
        #endregion
                        
        #region Methods



        #endregion
    }
}