using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.BeatEmUp
{
    /// <summary>
    /// Creation Date:   1/11/2022 11:18:09 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    public class BoundingBox : MonoBehaviour
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        [SerializeField] protected BEUObject _owner; 
        #endregion

        #region Properties, Consts and Statics
        public BEUObject Owner => _owner;
        private static List<BoundingBox> boundingboxes = new List<BoundingBox>();
        private static Dictionary<Collider, BoundingBox> cache = new Dictionary<Collider, BoundingBox>();
        [SerializeField] private Collider _collider;

        #endregion

        #region Variables
        
        #endregion

        #region Public API
        
        #endregion
                        
        #region Methods

        private void OnEnable()
        {
            Register(this);
        }

        private void OnDisable()
        {
            Unregister(this);
        }
        private static void Register(BoundingBox boundingBox)
        {
            boundingboxes.Add(boundingBox);
            cache.Add(boundingBox._collider, boundingBox);
        }


        private static void Unregister(BoundingBox boundingBox)
        {
            cache.Remove(boundingBox._collider);
            boundingboxes.Remove(boundingBox);
        }

        //[ContextMenu("call from context menu")]
        //public void Test() { }       

        #endregion

        public static BEUObject GetOwner(Collider o)
        {
            if (cache.ContainsKey(o))
            {
                var result = cache[o];
                return result._owner;
            }
            return null;
        }
    }
}