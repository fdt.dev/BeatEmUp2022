
using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   2/13/2022 11:59:41 AM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>

    [Juce.ImplementationSelector.SelectImplementationCustomDisplayName("Attack Connected")]
    public class AttackConnectedCondition : ConditionBase
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        
        #endregion
                        
        #region Methods

        public override bool CheckCondition(BEUFighter e)
        {
            var o =e.HitBox.CheckCollision();
            if (o != null && o != e && o.LastAttackReceived != e.HitBox.HitStateID)
            {
                return true;
            }
            return false;
        }
        #endregion
    }
}