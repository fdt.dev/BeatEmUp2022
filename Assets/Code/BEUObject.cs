using System;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.BeatEmUp
{
    /// <summary>
    /// Creation Date:   5/27/2021 10:31:57 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     This is the most basic object of the beat em up engine. This will be the parent of characters,
    ///                  objects and decoration
    /// Changelog:         
    /// </summary>
    public class BEUObject : MonoBehaviour
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        [SerializeField] protected Transform _grabPivot;
        
        private int _currentAttackDamage;
        [SerializeField] protected int _health;
        public int facingDirection = 1;
        //[Header("")]
        [SerializeField] protected SpriteRenderer _sprite;
        protected Hitbox _hitData;

        #endregion

        #region Properties, Consts and Statics
        public Transform GrabPivot => _grabPivot!=null?_grabPivot:transform;
        public bool HasBeenHit => _hitData != null && !consumedAttack;

        public bool consumedAttack = false;
        
        public int Health
        {
            get => _health;
            set => _health = value;
        }

        public Hitbox HitData
        {
            get => _hitData;
            set
            {
                _hitData = value;  
            } 
        }
        
        public int LastAttackReceived
        {
            get => _lastAttackReceived;
            set => _lastAttackReceived = value;
        }

        public int AttackDamage
        {
            get => _currentAttackDamage;
        }
        public void SetAttackDamage(int attackDamage)
        {
            _currentAttackDamage = attackDamage;
        }
        #endregion

        #region Variables

        protected int _lastAttackReceived = -1;
        #endregion

        #region Public API
        public void FaceDirection(float intentionX)
        {
            if (intentionX < 0)
            {
                _sprite.flipX = true;
                facingDirection = -1;
            }
            else if (intentionX > 0)
            {
                _sprite.flipX = false;
                facingDirection = 1;
            }
        }

        public void FacePoint(Vector3 p)
        {
            var dir = p - transform.position;
            FaceDirection(dir.x);
        }
        #endregion
                        
        #region Methods
        //[ContextMenu("call from context menu")]
        //public void Test() { }        
       
        #endregion

        public virtual void CollidedWithHitbox(Hitbox hitbox, BEUObject attacker)
        {
            if (hitbox.Owner.IsGrab)
            {
                GrabExecuted(hitbox);
            }
            else
            {
                AttackExecuted(hitbox);
            }
        }

        protected virtual void Update()
        {
            foreach (var o in _grabbedObjs)
            {
                o.UpdateGrabbed();
            }
        }

        private void GrabExecuted(Hitbox hitbox)
        {
            Debug.Log($"{gameObject.name} grabbed by {hitbox.Owner}");
        }
        private void AttackExecuted(Hitbox hitbox)
        {
            _health -= hitbox.Owner.AttackDamage;
            _hitData = hitbox;
            _lastAttackReceived = hitbox.HitStateID;
            consumedAttack = false;
            Debug.Log($"{gameObject.name} attacked by {hitbox.Owner}");
        }
        public bool IsGrab = false;

        public void ModHealth(int h)
        {
            _health += h;
        }
        public void SetLastReceivedAttack(Hitbox myHitbox)
        {
            _hitData = myHitbox;
            _lastAttackReceived = _hitData.HitStateID;
            consumedAttack = false;
        }

        protected BEUObject _grabber;
        public BEUObject Grabber => _grabber;
        protected List<BEUObject> _grabbedObjs = new List<BEUObject>();
        public List<BEUObject> GrabbedObjects => _grabbedObjs;
        public int GrabbedObjectsCount => _grabbedObjs.Count;
        
        public void SetGrabbedBy(BEUObject grabber)
        {
            _grabber = grabber;
        }

        public void UpdateGrabbed()
        {
            transform.position = _grabber.GrabPivot.position;
        }
        public void Grab(BEUObject beuObject)
        {
            _grabbedObjs.Add(beuObject);
        }
        
        public void DropGrabbed()
        {
            foreach (var o in _grabbedObjs)
            {
                o.SetGrabbedBy(null);
            }
            _grabbedObjs.Clear();
        }
    }
}