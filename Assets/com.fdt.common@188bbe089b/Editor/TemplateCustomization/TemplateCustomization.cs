using System;
using System.IO;
using UnityEngine;
using UnityEditor;

namespace com.FDT.Common.TemplateCustomization.Editor
{
    /// <summary>
    /// Creation Date:   23/02/2020 22:36:12
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// </summary>
    public class TemplateCustomization : UnityEditor.AssetModificationProcessor
    {
        #region Properties, Consts and Statics
        public static string ProductNameVar
        {
            get { return Application.dataPath + "_ProductName"; }
        }
        public static string NamespaceTextVar
        {
            get { return Application.dataPath + "_namespaceText"; }
        }
        public static string CompanyNameVar
        {
            get { return Application.dataPath + "_companyName"; }
        }
        public static string DevNameVar
        {
            get { return Application.dataPath + "_devName"; }
        }
        public static string ScriptableMenuVar
        {
            get { return Application.dataPath + "_scriptableMenu"; }
        }
        #endregion

        #region Methods
        public static void OnWillCreateAsset(string opath)
        {
            string path = opath.Replace(".meta", "");
            int index = path.LastIndexOf(".", StringComparison.InvariantCultureIgnoreCase);
            if (index == -1) return;
            string file = path.Substring(index);
            if (file != ".cs") return;

            string namespaceText = GetNamespaceText();

            string newPath = path.Substring(7);    // removes "Asset/"
            string fileName = Path.GetFileName(newPath);
            string plainFileName = fileName.Substring(0, fileName.Length - 3);

            index = Application.dataPath.LastIndexOf("Assets", StringComparison.InvariantCultureIgnoreCase);
            path = Application.dataPath.Substring(0, index) + path;
            file = System.IO.File.ReadAllText(path);
            
            if (file.EndsWith("MonoBehaviour\r\n{\r\n    // Start is called before the first frame update\r\n    void Start()\r\n    {\r\n        \r\n    }\r\n\r\n    // Update is called once per frame\r\n    void Update()\r\n    {\r\n        \r\n    }\r\n}\r\n"))
            {
                // comes from addcomponent in inspector
                var guid = AssetDatabase.FindAssets("81-C# Script-NewBehaviourScript.cs");
                if (guid.Length > 0)
                {
                    var f = AssetDatabase.LoadAssetAtPath<TextAsset>(AssetDatabase.GUIDToAssetPath(guid[0]));
                    if (f != null)
                    {
                        file = f.text;
                        file = BasicFillData(plainFileName, file);
                    }
                }
            }
            if (file.Contains("#NAMESPACE#"))
            {
                file = FillData(plainFileName, file);
                System.IO.File.WriteAllText(path, file);
                AssetDatabase.Refresh();
            }
        }

        public static string BasicFillData(string plainFileName, string file)
        {
            file = file.Replace("#NOTRIM#", String.Empty);
            file = file.Replace("#SCRIPTNAME#", plainFileName);
            return file;
        }

        public static string FillData(string className, string file)
        {
            string namespaceText = GetNamespaceText();
            file = file.Replace("#CREATIONDATE#", System.DateTime.Now + "");
            file = file.Replace("#PROJECTNAME#", GetProductName());
            file = file.Replace("#COMPANY#", GetCompanyName());
            file = file.Replace("#DEV#", GetDevName());
            file = file.Replace("#NAMESPACE#", namespaceText);
            file = file.Replace("#SCRIPTMENU#", GetScriptMenu() + "/" + className);
            file = file.Replace("#SCRIPTABLEFILE#", "New " + className);
            return file;
        }

        public static string GetScriptMenu()
        {
            return EditorPrefs.GetString(ScriptableMenuVar, "defaultMenu");
        }

        public static string GetProductName()
        {
            return EditorPrefs.GetString(ProductNameVar, PlayerSettings.productName);
        }

        public static string GetNamespaceText()
        {
            return EditorPrefs.GetString(NamespaceTextVar, "defaultNamespace");
        }

        public static string GetCompanyName()
        {
            return EditorPrefs.GetString(CompanyNameVar, PlayerSettings.companyName);
        }

        public static string GetDevName()
        {
            return EditorPrefs.GetString(DevNameVar, Environment.UserName);
        }
        #endregion
    }
}