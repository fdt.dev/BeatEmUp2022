using UnityEngine;

namespace StringFSMSystem
{
    /// <summary>
    /// Creation Date:   1/2/2022 11:19:35 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public abstract class BaseFSMState
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        //[SerializeField] 
        [SerializeField] protected string _stateID;
        #endregion

        #region Properties, Consts and Statics

        #endregion

        #region Variables

        #endregion

        #region Public API
        public string StateID
        {
            get => _stateID;
        }
        #endregion

        #region Methods

        //[ContextMenu("call from context menu")]
        //public void Test() { }        

        #endregion


    }
}