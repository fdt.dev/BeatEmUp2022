using MackySoft.SerializeReferenceExtensions.Editor;
using StringFSMSystem;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace com.FDT.BeatEmUp.FSM.Editor
{
    /// <summary>
    /// Creation Date:   1/3/2022 9:46:02 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     This is only needed for the project because main functionality derived from the usage
    ///                  of SerializeReference would result in shallow copies of classes and those cases had to be handled.
    /// Changelog:         
    /// </summary>
    [CustomPropertyDrawer(typeof(StringFSMBase), true)]
    public class StringFSMDrawer : PropertyDrawer
    {
        private ReorderableList _stateList;
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            SerializedProperty currentProp = property.FindPropertyRelative("currentState");
            SerializedProperty listProp = property.FindPropertyRelative("States");
            float currentHeight = EditorGUI.GetPropertyHeight(currentProp);
            float listHeight = EditorGUI.GetPropertyHeight(listProp);
            var list = GetList(listProp);
            list.DoList(new Rect(position.x, position.y + currentHeight + 2, position.width, listHeight));
            EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, currentHeight), currentProp, true);
            //EditorGUI.PropertyField(new Rect(position.x, position.y + currentHeight + 2, position.width, listHeight),
             //   listProp, true);
            
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            SerializedProperty currentProp = property.FindPropertyRelative("currentState");
            SerializedProperty listProp = property.FindPropertyRelative("States");
            float currentHeight = EditorGUI.GetPropertyHeight(currentProp);
            float listHeight = EditorGUI.GetPropertyHeight(listProp);
            return currentHeight + 2 + listHeight;
        }
        private ReorderableList GetList(SerializedProperty property)
        {
            if (_stateList == null)
            {
                _stateList = new ReorderableList(property.serializedObject, property);

                _stateList.drawHeaderCallback = (Rect rect) =>
                {
                    EditorGUI.LabelField(rect, "States");
                };

                _stateList.drawElementCallback =
                    (Rect rect, int index, bool isActive, bool isFocused) =>
                    {
                        SerializedProperty element = property.GetArrayElementAtIndex(index); // The element in the list
                        EditorGUI.PropertyField(rect, element, true); 
                    };
                _stateList.elementHeightCallback = index =>
                {
                    SerializedProperty element = property.GetArrayElementAtIndex(index); // The element in the list
                    return EditorGUI.GetPropertyHeight(element, true);
                };
                _stateList.onAddCallback = (ReorderableList list) =>
                {
                    int length = property.arraySize;
                    property.InsertArrayElementAtIndex(length);
                    var element = property.GetArrayElementAtIndex(length);
                    var state = element.FindPropertyRelative("_stateType");
                    state.SetManagedReference(typeof(CommonMovementState));
                    var conditions = element.FindPropertyRelative("_stateConditions");
                    conditions.arraySize = 0;
                    property.serializedObject.ApplyModifiedProperties();
                };
            }

            return _stateList;
        }
    }
}