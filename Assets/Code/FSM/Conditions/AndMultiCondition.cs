namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   1/3/2022 5:46:44 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    [Juce.ImplementationSelector.SelectImplementationCustomDisplayName("Multi Condition AND")]
    public class AndMultiCondition : MultiConditionBase
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        public override bool CheckConditions(BEUFighter e)
        {
            for (int i = 0; i < _conditions.Count; i++)
            {
                var c = _conditions[i];
                var r = c.CheckCondition(e);
                if (!r)
                {
                    return false;
                }
            }
            return true;
        }
        #endregion
                        
        #region Methods
        //[ContextMenu("call from context menu")]
        //public void Test() { }   

        #endregion
    }
}