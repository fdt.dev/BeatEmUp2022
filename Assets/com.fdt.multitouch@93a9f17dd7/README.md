# FDT Multitouch

MultiTouch system to easily deal with up to 5 touches.


## Usage

The package must be included using the manifest.json file in the project.
The file must be modified to include this dependencies:

```json
{
  "dependencies": {
	"com.fdt.multitouch": "https://gitlab.com/fdtmodules/fdtmultitouch.git#2021.1.0",
	"com.fdt.common": "https://gitlab.com/fdtmodules/fdtcommon.git#2021.1.0",

	...
  }
}

```

## License

MIT - see [LICENSE](https://gitlab.com/fdtmodules/fdtmultitouch/src/2021.1.0/LICENSE.md)