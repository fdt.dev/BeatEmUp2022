using UnityEngine;

namespace com.FDT.BeatEmUp
{
    /// <summary>
    /// Creation Date:   1/23/2022 10:01:06 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    public class RigidbodyWrapper : MonoBehaviour
    {
        #region Classes, Structs and Enums

        #endregion

        #region GameEvents and UnityEvents

        //[Header("")]
        //[SerializeField] 

        #endregion

        #region Actions, Delegates and Funcs

        #endregion

        #region Inspector Fields

        [SerializeField] protected Rigidbody rb;
        [SerializeField] protected bool _isGrounded = false;
        [SerializeField] protected bool _isKinematic = false;
        [SerializeField] protected float _gravityModifier = 1;
        
        #endregion

        #region Properties, Consts and Statics

        public float gravityModifier
        {
            get => _gravityModifier;
            set => _gravityModifier = value;
        }
        public Vector3 velocity
        {
            get => rb.velocity;
            set => rb.velocity = value;
        }

        public bool IsGrounded
        {
            get => _isGrounded;
            set => _isGrounded = value;
        }

        public bool IsKinematic
        {
            get => _isKinematic;
            set => _isKinematic = value;
        }
        #endregion

        #region Variables

        #endregion

        #region Public API

        #endregion

        #region Methods

        //[ContextMenu("call from context menu")]
        //public void Test() { }        

        #endregion

        public void SetTargetVelocity(Vector3 v)
        {
            rb.velocity = v;
        }

        public void ProcessVelocity(Vector3 v)
        {
            rb.MovePosition(rb.position + v);
        }

        public void SetIntention(Vector2 intention)
        {
            var vel = rb.velocity;
            vel.x = intention.x;
            vel.z = intention.y;
            rb.velocity = vel;
        }

        private void FixedUpdate()
        {
            rb.useGravity = false;
            if (!_isKinematic)
            {
                var vel = rb.velocity;
                vel += -Vector3.up * (_gravityModifier * test);
                rb.velocity = vel;
            }
            Debug.DrawLine(transform.position-offset, transform.position-offset + (-Vector3.up * groundedTest));
            _isGrounded = Physics.Raycast(transform.position-offset, -Vector3.up, groundedTest, _layerMask.value);
        }

        public Vector3 offset;

        public LayerMask _layerMask;
        public float groundedTest = .5f;
        public float test = 1;
    }
}