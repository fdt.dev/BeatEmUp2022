﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.fdt.multitouch
{
    public interface IMultiTouchHandler
    {
        bool IsOccupiedByOther(UnityEngine.Object o, int fingerId);
        void SetOccupied(UnityEngine.Object o, int fingerId);
        int GetOccupiedTouch(UnityEngine.Object o);
        void ReleaseOccupied(UnityEngine.Object o, int fingerId);
        TouchDataUEvt TouchUEvt { get; }
    }
}