using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   2/15/2022 11:08:10 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public class ApplyForceStateAction : StateActionBase
    {
        [SerializeField] protected Vector3 _force;
        public override void Execute(BEUFighter e)
        {
            Vector3 vector = _force;
            vector.x *= e.facingDirection;
            e.rb.SetTargetVelocity(vector);
        }
    }
}