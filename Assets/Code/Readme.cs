using UnityEngine;

namespace FDT
{
    /// <summary>
    /// Creation Date:   1/6/2022 8:19:26 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     Simple script with a textarea to leave info in the game hierarchy
    /// Changelog:         
    /// </summary>
    public class Readme : MonoBehaviour
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields

        [SerializeField, TextArea(1, 2000)] private string _text;

        #endregion

        #region Properties, Consts and Statics

        #endregion

        #region Variables

        #endregion

        #region Public API

        #endregion

        #region Methods

        //[ContextMenu("call from context menu")]
        //public void Test() { }        

        #endregion
    }
}