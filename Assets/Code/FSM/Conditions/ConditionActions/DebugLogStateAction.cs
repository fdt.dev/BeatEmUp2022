using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   2/9/2022 7:28:57 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public class DebugLogStateAction : StateActionBase
    {
        [SerializeField, TextArea] protected string _text;

        public override void Execute(BEUFighter e)
        {
            Debug.Log(_text);
        }
    }
}