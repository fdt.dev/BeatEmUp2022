using com.FDT.BeatEmUp;
using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   1/3/2022 4:13:44 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [Juce.ImplementationSelector.SelectImplementationCustomDisplayName("Move Vertical")]
    public class ConditionMoveVertical : ConditionBase
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        [SerializeField] protected bool _isMoving = false;

        #endregion

        #region Properties, Consts and Statics

        #endregion

        #region Variables

        #endregion

        #region Public API

        #endregion

        #region Methods

        public override bool CheckCondition(BEUFighter beuFighter)
        {
            bool movingV = Mathf.Abs(beuFighter.Intention.y) > InputAsset.DEADZONE;
            return movingV == _isMoving;
        }

        #endregion
    }
}