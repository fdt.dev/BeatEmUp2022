namespace com.FDT.Common
{
    /// <summary>
    /// Creation Date:   01/02/2020 22:51:22
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     original version available in http://baba-s.hatenablog.com/entry/2014/08/20/112256
    /// </summary>
    public enum HelpBoxType
    {
        None,
        Info,
        Warning,
        Error,
    }
}