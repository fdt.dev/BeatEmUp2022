using System.Collections.Generic;
using StringFSMSystem;
using UnityEngine;

namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   1/3/2022 4:00:58 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     This has the base logic for the fsm states
    /// Changelog:         
    /// </summary>
    [System.Serializable]
    public class BeatEmUpState : BaseFSMState
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields

        [Juce.ImplementationSelector.SelectImplementation(typeof(IStateAnimation), false, true)]
        [SerializeField, SerializeReference]
        protected IStateAnimation _animation;
        
        [SerializeField] protected bool _canFlip;
        
        [SerializeField] protected float _runSpeed = 1;
        
        [Juce.ImplementationSelector.SelectImplementation(typeof(IStateType), false, true)]
        [SerializeField, SerializeReference]
        protected IStateType _stateType;
                
        [Juce.ImplementationSelector.SelectImplementation(typeof(IStateAction), false, true)]
        [SerializeField, SerializeReference]
        protected List<IStateAction> _startActions = new List<IStateAction>();
        
        [Juce.ImplementationSelector.SelectImplementation(typeof(IStateAction), false, true)]
        [SerializeField, SerializeReference]
        protected List<IStateAction> _updateActions = new List<IStateAction>();
        
        [Juce.ImplementationSelector.SelectImplementation(typeof(IStateAction), false, true)]
        [SerializeField, SerializeReference]
        protected List<IStateAction> _endActions = new List<IStateAction>();

        [Juce.ImplementationSelector.SelectImplementation(typeof(IStateConditionType), false, true)]
        [SerializeField, SerializeReference]
        protected List<IStateConditionType> _stateConditions = new List<IStateConditionType>();

        
        #endregion

        #region Properties, Consts and Statics

        public bool CanFlip => _canFlip;

        public float RunSpeed => _runSpeed;

        #endregion

        #region Variables

        #endregion

        #region Public API

        public bool CheckConditions(BEUFighter e, out string newState)
        {
            newState = null;
            for (int i = 0; i < _stateConditions.Count; i++)
            {
                var c = _stateConditions[i];
                if (c.CheckConditions(e))
                {
                    c.ExecuteConditionActions(e);
                    c.ExecuteConditionSuccess(e);
                    return true;
                }
            }
            return false;
        }
        public void InitState(BEUFighter e)
        {
            _stateType.InitState(e);
            for (int i = 0; i < _startActions.Count; i++)
            {
                _startActions[i].Execute(e);
            }
        }
        public void ExecuteState(BEUFighter e)
        {
            _stateType.ExecuteState(e, this);
            for (int i = 0; i < _updateActions.Count; i++)
            {
                _updateActions[i].Execute(e);
            }
        }
        #endregion

        #region Methods

        //[ContextMenu("call from context menu")]
        //public void Test() { }        
        #endregion

        public void CollidedWithHitbox(Hitbox myHitbox, BEUFighter e, BEUObject attacker)
        {
            _stateType.CollidedWithHitbox(myHitbox, e, attacker);
        }

        public void EndState(BEUFighter e)
        {
            for (int i = 0; i < _endActions.Count; i++)
            {
                _endActions[i].Execute(e);
            }
        }

        public void PlayAnimation(BEUAnimatorHook anim)
        {
            _animation.Execute(anim);
        }
    }
}