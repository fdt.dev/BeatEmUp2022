using System.Collections.Generic;
using com.FDT.Common;
using UnityEngine;

namespace com.FDT.BeatEmUp
{
    [UnitySingleton(UnitySingletonAttribute.Type.ExistsInScene, true)]
    public class CameraHandler : Singleton<CameraHandler>
    {
        [SerializeField] protected Transform _camMain;
        [SerializeField] protected Camera _cam;
        [SerializeField] protected float _min = 0;
        [SerializeField] protected float _max = 100;

        [SerializeField] protected List<Transform> _followTargets = new List<Transform>();
        [SerializeField] protected float _smoothTime = 0.1f;
        [SerializeField] protected float _maxVel = 5;
        [SerializeField] protected float _cameraOffset = 5;
        private Vector3 _currentVel;
        
        public float Min => _min;
        public float Max => _max;
        public Camera Cam => _cam;
        
        private void Update()
        {
            Clamp();
        }

        private void FixedUpdate()
        {
            Clamp();
        }

        private void LateUpdate()
        {
            Clamp();
        }
        private void Clamp()
        {
            if (_followTargets.Count == 0)
                return;

            var newTargetPosition = GetTargetPos(_followTargets);
            var desiredCamPos = _camMain.transform.position;
            var oldCamPosX = desiredCamPos.x;
            
            desiredCamPos.x = newTargetPosition.x;
            desiredCamPos.z = _camMain.transform.position.z;
            Vector3 currentVelocity = Vector3.zero;
            _camMain.transform.position = Vector3.SmoothDamp(_camMain.transform.position, desiredCamPos, ref currentVelocity, _smoothTime, _maxVel);
            var p = _camMain.transform.position;
            if (currentVelocity.x < 0)
            {
                if (p.x < _min)
                {
                    p.x = oldCamPosX;
                }
                else
                {
                    p.x = Mathf.Max(_min, p.x);
                }
            }
            else if (currentVelocity.x > 0)
            {
                if (p.x > _max)
                {
                    p.x = oldCamPosX;
                }
                else
                {
                    p.x = Mathf.Min(_max, p.x);
                }
            }
            _camMain.transform.position = p;
        }

        public Vector3 GetTargetPos(List<Transform> transforms)
        {
            var bound = new Bounds(transforms[0].position, Vector3.zero);
            for(int i = 1; i < transforms.Count; i++)
            {
                bound.Encapsulate(transforms[i].position);
            }
            return bound.center;
        }
        public void SetMinMax(float min, float max)
        {
            _min = min;
            _max = max;
        }
        private void OnDrawGizmosSelected()
        {
            Color oldColor = Gizmos.color;
            var p = Vector3.zero;
            p.y = 15;
            p.x = _min - _cameraOffset;
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(p, p + (Vector3.down * 30));
            
            p.x = _max + _cameraOffset;
            Gizmos.color = Color.green;
            Gizmos.DrawLine(p, p + (Vector3.down * 30));

            Gizmos.color = oldColor;
            
        }
    }
}