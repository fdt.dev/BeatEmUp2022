namespace com.FDT.BeatEmUp.FSM
{
    /// <summary>
    /// Creation Date:   1/3/2022 4:02:30 PM
    /// Product Name:    BeatEmUp2022
    /// Developers:      FDT Dev
    /// Company:         FDT
    /// Description:     
    /// Changelog:         
    /// </summary>
    [Juce.ImplementationSelector.SelectImplementationDefaultType]
    public interface IStateConditionType
    {
        bool CheckConditions(BEUFighter beuFighter);
        void ExecuteConditionActions(BEUFighter beuFighter);
        void ExecuteConditionSuccess(BEUFighter beuFighter);
    }
}